//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = ""
    let posOdd=[];
    let negEven=[];
    let myNum=[54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25]
    for(i=0; i<myNum.length; i++){
        if(myNum[i] % 2===1 && myNum[i]>0){
            posOdd.push(myNum[i])
        } else if(myNum[i] % 2===0 && myNum[i]<0){
            negEven.push(myNum[i])
        }else{
            continue;
        }
    }
    output+= "Positive Odd : " + posOdd + "\n"
    output+= "Negative Even : " + negEven + "\n"
   
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output  //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    //6 variables for each dice number 
    let face1=0; let face2=0; let face3=0; let face4=0; let face5=0; let face6=0;
    
    output += "Frequency of dice rolls" + "\n"
    
    let myDice;
    for(i=1;i<=60000;i++){
    myDice = Math.floor(Math.random()*6)+1
     if (myDice === 1){
        face1++
       }else if(myDice === 2){
        face2++
       }else if(myDice === 3){
        face3++
       }else if(myDice === 4){
        face4++
       }else if(myDice === 5){
        face5++
       }else {
        face6++
       }
     }
    
    output+= 1 + ":" + face1 + "\n"
    output+= 2 + ":" + face2 + "\n"
    output+= 3 + ":" + face3 + "\n"
    output+= 4 + ":" + face4 + "\n"
    output+= 5 + ":" + face5 + "\n"
    output+= 6 + ":" + face6 + "\n"
    

    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    let diceArray=[0,0,0,0,0,0,0]//assign an empty array to store the value later
    let myDice;
    for(i=1;i<=60000;i++){
      myDice=Math.floor(Math.random()*6)+1
      diceArray[myDice]++
    } 
    output+="Frequency of dice rolls\n"
    for(i=1;i<=6;i++){
        output+= i + " : " + diceArray[i] + "\n"
    }
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    var dieRolls = {
        Frequencies: {
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        Total:60000,
        Exceptions: ""
    }
    
    let myDice; //assign variable for the result of the dice 
    for (i=1;i<=dieRolls.Total;i++){
        myDice=Math.floor(Math.random()*6)+1
        dieRolls.Frequencies[myDice]=dieRolls.Frequencies[myDice]+1 //add the number of times the number appears
    } 
    
    output+="Frequency of dice rolls\n"
    output+="Total rolls : " + dieRolls.Total + "\n"
    output+="Frequencies :\n "
    for(i=1;i<=6;i++){
       output+= i + " : " + dieRolls.Frequencies[i] + "\n"//output of the appearance of each number of dice
       //find the Exceptions
       if(dieRolls.Frequencies[i]>10000*1.01||dieRolls.Frequencies[i]<10000*(0.99)){
            dieRolls.Exceptions+=i + " "
       }
    }
    output+="Exceptions : " + dieRolls.Exceptions + "\n"

    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    let person = {
        name : "Jane",
        income : 127050
    }
    let tax;
    if(person.income<=18200){
        tax = 0;
    }else if(person.income>18200 && person.income<=37000){
        tax=0.19*(person.income-18200) 
    }else if(person.income>37000 && person.income<=90000){
        tax=3572 + 0.325*(person.income-37000)
    }else if(person.income>90000 && person.income<=180000){
        tax=20797 + 0.37*(person.income-90000)
    }else if(person.income>180000){
        tax=54097 + 0.45*(person.income-180000)
    }
    output=person.name + "'s income is : $" + person.income + ", and her tax owed is : $" + tax.toFixed(2)
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}